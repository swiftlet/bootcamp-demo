Rails.application.routes.draw do
  resources :posts
  resources :users

  resource :playground, controller: 'playground' do
    collection do
      get :demo2
    end
  end

  root 'posts#index'
end
