var demo2Ctrl = function($scope) {
  $scope.plus = function() {
    $scope.result = parseInt($scope.a) + parseInt($scope.b);
  }

  $scope.minus = function() {
    $scope.result = $scope.a - $scope.b;
  }

  $scope.multiply = function() {
    $scope.result = $scope.a * $scope.b;
  }

  $scope.divide = function() {
    $scope.result = $scope.a / $scope.b;
  }

  $scope.mod = function() {
    $scope.result = $scope.a % $scope.b;
  }
}

demo2Ctrl.$inject = ["$scope"];
angular.module('Twitter').controller("Demo2Controller", demo2Ctrl);
