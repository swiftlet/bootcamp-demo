//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require angular
//= require_self
//= require_tree .

angular.module('Twitter', []);

var logic = {
  "plus": function(a, b) {
    return a + b;
  },
  "multiply": function(a, b) {
    return a * b;
  },
  "divide": function(a, b) {
    return a / b;
  },
  "mod": function(a, b) {
    return a % b;
  },
  "minus": function(a, b) {
    return a - b;
  }
}

function init() {
  $(".btn-operator").on('click', function() {
    var a = parseInt($("#a").val());
    var b = parseInt($("#b").val());
    var operator = $(this).data('operator');
    var result = logic[operator](a, b);

    if(result != "NaN" && result != "" && result != null && result != undefined && result != "Infinity" && result) {
      $("#result_wrapper").show();
    }
    else {
      $("#result_wrapper").hide();
    }

    $("#result").text(result);
  });

}

$(document).ready(init);
