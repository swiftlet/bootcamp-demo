class User < ActiveRecord::Base
  has_many :posts
  has_many :relationships,
    foreign_key: 'user_id'

  has_many :followings,
    through: :relationships,
    source: 'following'

end
